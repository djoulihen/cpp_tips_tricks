#include <cstdint>
#include <cstring>
#include <iostream>
#include <utility>

// Example of a class that contains heavy data and that we should avoid copying
class ContainsHeavyData
{
public:

    // Default constructor: allocates the heavy data
    ContainsHeavyData()
    {
        m_heavyData = new char[heavyDataSize];
        std::cout << "Default constructor, data at addr " << static_cast<void *>(m_heavyData) << std::endl;
    }

    // Copy constructor: allocates and copies the heavy data from other
    ContainsHeavyData(ContainsHeavyData const& other)
    {
        m_heavyData = new char[heavyDataSize];
        ::memcpy(m_heavyData, other.m_heavyData, heavyDataSize);
        std::cout << "Copy constructor, data at addr " << static_cast<void *>(m_heavyData) << std::endl;
    }

    // Move constructor: steals heavy data from other
    ContainsHeavyData(ContainsHeavyData&& other)
    {
        std::swap(m_heavyData, other.m_heavyData);
        std::cout << "Move constructor, data at addr " << static_cast<void *>(m_heavyData) << std::endl;
    }

    ~ContainsHeavyData()
    {
        // Ok to delete[] nullptr (if its data was stolen)
        delete[] m_heavyData;
    }

    static constexpr size_t heavyDataSize = 512;

    char *m_heavyData = nullptr;
};

// Passing by copy should be avoided unless for primitive types (int etc.)
void PassByCopy(ContainsHeavyData /*data*/) {}

// Always pass by const reference if not modifying the parameter
void PassByReference(ContainsHeavyData const& /*data*/) {}

// Global variables should be avoided but this is for the sake of this example
ContainsHeavyData myGlobalData;

// Function returning by reference
ContainsHeavyData const& GetGlobalData()
{
    return myGlobalData;
}

// Function returning by copy
ContainsHeavyData CreateNewData()
{
    ContainsHeavyData myData;
    return myData;
}

int main()
{
    std::cout << std::endl << "Calling PassByCopy" << std::endl;
    PassByCopy(myGlobalData);
    std::cout << "Called PassedByCopy" << std::endl << std::endl;

    std::cout << "Calling PassByReference" << std::endl;
    PassByReference(myGlobalData);
    std::cout << "Called PassByReference" << std::endl << std::endl;

    std::cout << "Calling GetGlobalData" << std::endl;
    ContainsHeavyData const& dataRef = GetGlobalData();
    std::cout << "Called GetGlobalData" << std::endl << std::endl;

    std::cout << "Calling CreateNewData" << std::endl;
    // The compiler may already optimize this on its own (return value optimization)
    ContainsHeavyData newData = CreateNewData();
    std::cout << "Called CreateNewData" << std::endl << std::endl;





    // Here we call passbycopy with an r-value (temporary variable)
    std::cout << std::endl << "Calling PassByCopy (r-value)" << std::endl;
    // The compiler may already optimize this on its own (copy elision)
    PassByCopy(ContainsHeavyData());
    std::cout << "Called PassedByCopy (r-value)" << std::endl << std::endl;

    // Here we call passbycopy with another r-value (return value from a function)
    std::cout << std::endl << "Calling PassByCopy (CreateNewData())" << std::endl;
    PassByCopy(CreateNewData());
    std::cout << "Called PassedByCopy (CreateNewData())" << std::endl;

    // Here we call passbycopy by making an l-value (long-lived, assignable variable) into an r-value
    std::cout << std::endl << "Calling PassByCopy (std::move)" << std::endl;
    PassByCopy(std::move(myGlobalData));
    std::cout << "Called PassedByCopy (std::move)" << std::endl;
    std::cout << "myGlobalData now has data at addr "
              << static_cast<void *>(myGlobalData.m_heavyData) << std::endl << std::endl;

    return 0;
}