#include <iostream>
#include <stdexcept>
#include <vector>


// --------------------------------- data -------------------------------------

struct AllocatedData
{
    AllocatedData()
    {
        std::cout << "AllocatedData constructed - " << this << std::endl;
    }

    ~AllocatedData()
    {
        std::cout << "AllocatedData destructed - " << this << std::endl;
    }

    void ExceptionThrowingMethod(bool throwException)
    {
        if (throwException)
            throw std::runtime_error("FAIL");
    }
};

// Function using raw pointers
void AllocateAndFreeData(bool throwException)
{
    AllocatedData* data = new AllocatedData;
    std::cout << "Allocated data" << std::endl;

    std::cout << "Calling ExceptionThrowingMethod" << std::endl;
    data->ExceptionThrowingMethod(throwException);
    std::cout << "Called ExceptionThrowingMethod" << std::endl;

    delete data;
    std::cout << "Freed data" << std::endl;
}

// Function using smart pointers (unique_ptr)
void AllocateAndFreeSmartData(bool throwException)
{
    // unique_ptr has ownership of the pointer and cannot be copied
    std::unique_ptr<AllocatedData> data(new AllocatedData);
    std::cout << "Allocated data" << std::endl;

    // The pointer can also be cast to bool to test if it's null
    // The raw pointer can be accessed with get()
    if (!data) // is equivalent to (data.get() == nullptr)
    {
        std::cerr << "NULL allocation, something's wrong" << std::endl;
        return;
    }

    std::cout << "Calling ExceptionThrowingMethod" << std::endl;
    // The -> and * operators work as with raw pointers
    data->ExceptionThrowingMethod(throwException);
    std::cout << "Called ExceptionThrowingMethod" << std::endl;
}

// unique_ptr can also be used as a member variable as shown here
// In that case the data will be automatically freed when the class is destructed
class ContainsAllocatedData
{
public:
    ContainsAllocatedData() : m_allocatedData(new AllocatedData) {}
private:
    std::unique_ptr<AllocatedData> m_allocatedData;
};

// shared_ptr can be used to share access to the data
class ContainsMultipleAllocatedData
{
public:
    ContainsMultipleAllocatedData(size_t numberOfData)
    {
        m_allocatedDataVector.reserve(numberOfData);
        for (size_t i = 0; i < numberOfData; ++i)
        {
            m_allocatedDataVector.emplace_back(new AllocatedData);
        }
    }

    // Here a copy of the pointer is returned
    std::shared_ptr<AllocatedData> GetData(size_t i) { return m_allocatedDataVector[i]; }

private:
    std::vector<std::shared_ptr<AllocatedData>> m_allocatedDataVector;
};


// Example for circular dependency of shared_ptr
struct Data1;

struct SubData1
{
    SubData1(std::shared_ptr<Data1> data1) : m_data1(data1)
    {
        std::cout << "SubData1 constructed - " << this << std::endl;
    }

    ~SubData1()
    {
        std::cout << "SubData1 destructed - " << this << std::endl;
    }

    std::shared_ptr<Data1> m_data1;
};

struct Data1
{
    Data1()
    {
        std::cout << "Data1 constructed - " << this << std::endl;
    }

    ~Data1()
    {
        std::cout << "Data1 destructed - " << this << std::endl;
    }

    std::shared_ptr<SubData1> m_subData1;
};


// Example with weak_ptr
struct Data2;

struct SubData2
{
    SubData2(std::weak_ptr<Data2> data2) : m_data2(data2)
    {
        std::cout << "SubData2 constructed - " << this << std::endl;
    }

    ~SubData2()
    {
        std::cout << "SubData2 destructed - " << this << std::endl;
    }

    std::weak_ptr<Data2> m_data2;
};

struct Data2
{
    Data2()
    {
        std::cout << "Data2 constructed - " << this << std::endl;
    }

    ~Data2()
    {
        std::cout << "Data2 destructed - " << this << std::endl;
    }

    std::shared_ptr<SubData2> m_subData2;
};

// --------------------------------- main -------------------------------------

int main()
{
    // -------------------------- raw pointers --------------------------------
    // First call without any exception called, all is well
    std::cout << "Call AllocateAndFreeData without exception" << std::endl;
    AllocateAndFreeData(false);
    std::cout << std::endl;

    // Second call with exception
    std::cout << "Call AllocateAndFreeData with exception" << std::endl;
    // As we are smart, we catch exceptions in case they happen. But we do it here which means the exception will
    // berak the flow of AllocateAndFreeData
    try
    {
        AllocateAndFreeData(true);
    }
    catch(const std::exception& e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
    }
    std::cout << std::endl;
    // We now have leaked memory as data hasn't been freed
    // Of course we could catch the exception in AllocateAndFreeData and rethrow it but that would be very cumbersome
    // to do for everyfunction. So in order to be even smarter, let's use RAII and smart pointers.


    // -------------------------- std::unique_ptr -----------------------------

    // First call without any exception called, all is well
    std::cout << "Call AllocateAndFreeSmartData without exception" << std::endl;
    AllocateAndFreeSmartData(false);
    std::cout << std::endl;

    // Second call with exception, all is well now :)
    std::cout << "Call with exception" << std::endl;
    try
    {
        AllocateAndFreeSmartData(true);
    }
    catch(const std::exception& e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
    }
    std::cout << std::endl;


    // -------------------------- std::shared_ptr -----------------------------

    // unique_ptr cannot be copied, so it's not meant to share access to the data. For that, use shared_ptr
    // shared_ptr keeps a count of copies to the data and only frees it when all copies are deleted. It can be seen
    // as a kind of garbage collection.

    {
        std::shared_ptr<AllocatedData> dataPtr;
        {
            // Create a new class containing several instances of Allocated data
            std::cout << "Creating ContainsMultipleAllocatedData containing 4 data" << std::endl;
            ContainsMultipleAllocatedData data4(4);

            std::cout << "Getting 3rd data" << std::endl;
            dataPtr = data4.GetData(2);

            std::cout << "Leaving ContainsMultipleAllocatedData scope" << std::endl;
        }

        // dataPtr still points to valid data even though data4 does not exist anymore.
        dataPtr->ExceptionThrowingMethod(false);

        std::cout << "Leaving dataPtr scope" << std::endl;
    }
    std::cout << std::endl;

    // WARNING: avoid creating circular dependency between shared_ptr otherwise you will leak memory.
    // Example with a data containing a subdata that references its main data
    {
        std::cout << "Creating Data1" << std::endl;
        std::shared_ptr<Data1> data1(new Data1);
        std::shared_ptr<SubData1> subData1(new SubData1(data1));
        data1->m_subData1 = subData1;

        std::cout << "Leaving data1 scope" << std::endl;
    }
    std::cout << std::endl;
    // Even though our shared_ptr was destroyed, the memory was not released because data1 and subData1 each have a
    // pointer referencing the other

    // WARNING: you should never create 2 shared_ptr from the same pointer, like this:
    // std::shared_ptr<Data> dataPtr(new Data);
    // std::shared_ptr<Data> dataPtr2(dataPtr.get());
    // This will create 2 separate counts and will lead to the pointer being freed twice. Always create a single
    // shared_ptr and copy it :
    // std::shared_ptr<Data> dataPtr(new Data);
    // std::shared_ptr<Data> dataPtr2 = dataPtr;

    // --------------------------- std::weak_ptr ------------------------------

    // To avoid circular dependency issues use weak_ptr. This is a pointer that can be created from a shared_ptr but
    // does not count when counting the number of references to a pointer;
    {
        std::cout << "Creating Data2" << std::endl;
        std::shared_ptr<Data2> data2(new Data2);
        std::shared_ptr<SubData2> subData2(new SubData2(data2));
        data2->m_subData2 = subData2;

        // Direct access to the object pointed to by a weak_ptr is not possible. It first needs to be converted to a
        // shared_ptr. Note that it is possible that the object has already been deleted so the pointer must be
        // checked.
        std::shared_ptr<Data2> data2ViaSubData2 = subData2->m_data2.lock();
        if (!data2ViaSubData2)
        {
            std::cout << "Object pointed to by weak_ptr has already been deleted !" << std::endl;
        }

        std::cout << "Leaving Data2 scope" << std::endl;
    }

    // Last note: cv::Mat is actually a smart pointer to the data. A lot of operations (sub-views etc.) do not copy the
    // data but create a new cv::Mat pointing to it. The data is freed only when all cv::Mat pointing to it are
    // destroyed.

    return 0;
}