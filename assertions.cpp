#include <cassert>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

// This function concatenates adresses and ports into a string. It's not a public API that is shipped to users, it's
// only used internally in our own codebase. And it expects adresses and ports to have the same size
std::string MyInternalFunctionWhichConcatenatesAddressesAndPorts(
    std::vector<std::string> const& adresses,
    std::vector<int> const& ports
)
{
    // assert is used to make use that some prerequisite we assume to be true is enforced
    // It's only executed when NDEBUG is not defined (usually not defined in Debug builds, defined in Release builds)
    // Can catch a programming error early and more clearly but does not slow down the Release build
    // assert(adresses.size() == ports.size());

    std::stringstream ss;
    for (size_t i = 0; i < adresses.size(); ++i)
    {
        ss << adresses[i] << ":" << ports[i] << ";";
    }
    return ss.str();
}

int main()
{
    std::vector<std::string> adresses = { "127.0.0.1", "192.168.1.1"};
    std::vector<int> ports = { 22, 9090 };

    // First test with both sizes equal, all is well
    std::cout << "Call with 2 adresses and 2 ports" << std::endl;
    std::cout << MyInternalFunctionWhichConcatenatesAddressesAndPorts(adresses, ports) << std::endl << std::endl;

    // Second test with more ports than adresses
    ports.push_back(45);
    std::cout << "Call with 2 adresses and 3 ports" << std::endl;
    std::cout << MyInternalFunctionWhichConcatenatesAddressesAndPorts(adresses, ports) << std::endl << std::endl;
    // Since adresses.size() is used to iterate, we just discard the extra ports and everything works.
    // But we are not alerted that there is a bug in our code (more ports than adresses). Our string only partial
    // information so we might end up we a bigger problems later and tracking it back to here may take some time.
    // We using asserts can help us catch bugs earlier and in a clearer way.

    // Third test with more adresses than ports
    adresses.push_back("0.0.0.0");
    adresses.push_back("192.168.0.1");
    std::cout << "Call with 4 adresses and 3 ports" << std::endl;
    std::cout << MyInternalFunctionWhichConcatenatesAddressesAndPorts(adresses, ports) << std::endl << std::endl;
    // Here it's even worse. We access ports that don't exist. This means the program could crash, or worse use
    // some random value that happens to be in memory at that place.
    // In Debug we are lucky that vector actually uses assert for indexes, so we see the error right away. But having
    // our own assert makes it even clearer and safer.

    // WARNING: Since assert is a macro, never count on code inside the macro to be executed. For example:
    int* myPointer = nullptr;
    assert((myPointer = new int) != nullptr);
    int value = *myPointer; // Will crash in Release

    // Note: if you need to do a more complicated check that does not fit into a macro and that you do not want
    // executed in Release builds, use #ifndef NDEBUG
    // Let's check that no value is present more than 3 times in a vector. We know it's supposed to be that way, but
    // we want to check anyway just in case
    // We enclose our test in #ifndef NDEBUG to avoid running expensive testing code in Release
    std::vector<int> myInts = {1, 1, 1, 2, 5, 0};
#ifndef NDEBUG
    std::unordered_map<int, unsigned int> counts;
    for (int val: myInts)
    {
        ++counts[val];
    }

    for (auto it: counts)
    {
        assert(it.second <= 3);
    }
#endif

    // Use static_assert when possible to check for bugs at compile time
    struct MyStruct
    {
        std::uint32_t data;
    };

    // static_assert needs a message prior to C++17
    static_assert(sizeof(MyStruct) <= sizeof(std::uint32_t), "MyStruct should be no larger than 32 bits");

    // Other use cases for assert can be checking that a called function had an expected effect:
    // ProcessAndClearData(data);
    // assert(data.empty());
    // In that case if someone modifies ProcessAndClearData() but did not know you expected data to be empty after,
    // you'll catch the bug pretty quickly

    // Or checking that a complicated piece of code did its work as you expected
    // std::vector<int> data = {1, 2, 8, 4, 9, 3};
    // [Complicated new algorithm for sorting data]
    // assert(IsSorted(data));

    // Or checking that you covered all cases
    enum class ParamType{ Int, Double };
    ParamType paramType = ParamType::Int;
    switch (paramType)
    {
        case ParamType::Int:
            // DoIntStuff();
            break;
        case ParamType::Double:
            // DoDoubleStuff();
            break;
        default:
            // Should never be reached but if someone adds a new param type and forgets to handle it here they'll know
            assert(false);
    }

    // Final notes:
    // - Assertions are a quick and easy way to add testing to an application but ideally you want to add unit tests
    // on top of that
    // - Assertions should never be used to replace proper handling of errors. In particular they should not be used
    // to check data coming from the user, from the disk, from a DB etc. They should only be used to test the internal
    // logic of the code.

    return 0;
}